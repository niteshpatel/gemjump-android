package com.herodevelop;

import android.app.Activity;
import com.android.vending.billing.util.IabHelper;
import com.android.vending.billing.util.IabHelper.OnConsumeFinishedListener;
import com.android.vending.billing.util.IabHelper.OnIabPurchaseFinishedListener;
import com.android.vending.billing.util.IabHelper.OnIabSetupFinishedListener;
import com.android.vending.billing.util.IabHelper.QueryInventoryFinishedListener;
import com.android.vending.billing.util.IabResult;
import com.android.vending.billing.util.Inventory;
import com.android.vending.billing.util.Purchase;
import com.herodevelop.hdfw.Billable;

public class Billing implements Billable {

    public IabHelper iabHelper;
    Activity activity;
    OnIabSetupFinishedListener iabSetupFinishedListener;
    OnIabPurchaseFinishedListener iabPurchaseFinishedListener;
    QueryInventoryFinishedListener queryInventoryFinishedListener;
    OnConsumeFinishedListener consumeFinishedListener;
    boolean iabHelperSetup;

    public Billing(Activity activity) {
        super();

        // Store reference to main activity
        this.activity = activity;

        // Create IabHelper instance
        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAg/i1eR6UrYQk4UH8nibZdrXK+fEpv01RHIK/styAjj6BRu6k+3OTdsSTEdiJ+p7t/5P7V9EoEwMAo+CSgHmHHgBVAzK0CKUF/fF3dD5E4ye/H6H4EuRdCYV34gDsimJm7FsIQJGpnWF17GTJG/5eNAB9hFdU0XdfUNtbPpQC57tPelPQKHK+PT5jmPPsCSse54R0w13aYl3jvuHRiTUKPl6BkwoczKKRQzAXBD5NR2omXxJKQM48Fdph+4MSw/8PA84L4koUbrNC7O0y0BlR/mLYZul2k+seV581WxHZvAmwW+JGQgHJ7ckD+JS5W6B+CfGzahX42Mi8Ll+bOS3KmwIDAQAB";
        iabHelper = new IabHelper(activity, base64EncodedPublicKey);
        iabHelperSetup = false;

        // Set up setup listener
        iabSetupFinishedListener = new

                IabHelper.OnIabSetupFinishedListener() {
                    public void onIabSetupFinished(IabResult result) {
                        iabHelperSetup = true;
                    }
                };


        // Set up the purchase listener
        iabPurchaseFinishedListener = new

                IabHelper.OnIabPurchaseFinishedListener() {
                    public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
                        onGdxIabPurchaseFinished(result, purchase);
                    }
                };

        // Set up the inventory listener
        queryInventoryFinishedListener = new

                IabHelper.QueryInventoryFinishedListener() {
                    public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
                        if (result.isFailure()) {
                        } else if (inventory.hasPurchase("android.test.purchased")) {
                            Purchase purchase = inventory.getPurchase("android.test.purchased");
                            iabHelper.consumeAsync(purchase, consumeFinishedListener);
                        }
                    }
                };

        // Set up consumption listener
        consumeFinishedListener = new

                IabHelper.OnConsumeFinishedListener() {
                    public void onConsumeFinished(Purchase purchase, IabResult result) {
                        if (result.isSuccess()) {
                            onGdxConsumeFinished();
                        } else {
                        }
                    }
                };
    }

    @Override
    public void startSetup() {
        iabHelper.startSetup(iabSetupFinishedListener);
    }

    @Override
    public void queryInventoryAsync() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                iabHelper.queryInventoryAsync(queryInventoryFinishedListener);
            }
        }
        );
    }

    public void onGdxQueryInventoryFinished() {
    }

    @Override
    public void consumeAsync() {
    }

    public void onGdxConsumeFinished() {
    }

    public void launchPurchaseFlow() {
        if (!isAsyncInProgress()) {
            iabHelper.launchPurchaseFlow(activity, "android.test.purchased", 1, iabPurchaseFinishedListener, "");
        }
    }

    public void onGdxIabPurchaseFinished(IabResult result, Purchase purchase) {
        if (result.isSuccess()) {
            iabHelper.consumeAsync(purchase, consumeFinishedListener);
        }
    }

    @Override
    public boolean isAsyncInProgress() {
        return iabHelper.mAsyncInProgress;
    }

    public boolean isIabHelperSetup() {
        return iabHelperSetup;
    }

    public void destroy() {
        if (iabHelper != null) iabHelper.dispose();
        iabHelper = null;
    }
}
