package com.herodevelop.gemjump;

import android.content.Intent;
import android.os.Bundle;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.herodevelop.Billing;

public class AndroidStarter extends AndroidApplication {
    MainStarter mainStarter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
        cfg.useAccelerometer = false;
        cfg.useCompass = false;
        cfg.useWakelock = false;
        cfg.useGL20 = true;

        // Create billing helper
        mainStarter = new MainStarter();
        mainStarter.billing = new Billing(this);
        initialize(mainStarter, cfg);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // Clean up billing connection
        ((Billing) mainStarter.billing).destroy();
    }

    // This allows us to handle the async returns from the billing purchasing process
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!((Billing) mainStarter.billing).iabHelper.handleActivityResult(requestCode, resultCode, data)) {

            // If the billing handler didn't handle the result, then defer to the default handling
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
